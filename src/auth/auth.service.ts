import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma.service';
import { Prisma } from '@prisma/client';
import { hashPassword } from 'src/helpers/bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private prisma: PrismaService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: Prisma.UserCreateInput) {
    const payload = { email: user.email, password: user.password };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async create(user: Prisma.UserCreateInput) {
    const payload = {
      email: user.email,
      password: await hashPassword(user.password),
    };
    try {
      const newUser = await this.prisma.user.create({
        data: payload,
      });
      this.jwtService.sign(newUser);

      return newUser;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
  async delete(id: number) {
    console.log(id);

    try {
      const newUser = await this.prisma.user.delete({
        where: { id },
      });
      this.jwtService.sign(newUser);

      return newUser;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
