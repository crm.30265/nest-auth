import {
  Controller,
  Get,
  Delete,
  Request,
  Post,
  UseGuards,
  Param,
} from '@nestjs/common';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
// import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { Public } from './decorators/public.decorator';

// Отже, ви повинні використовувати JwtAuthGuard, коли хочете захистити маршрути, які вимагають JWT автентифікації, і LocalAuthGuard, коли хочете захистити маршрути, які вимагають ім'я користувача та пароль для автентифікації.

@Controller('auth')
export class AppController {
  constructor(private authService: AuthService) {}

  @Public()
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.body.user);
  }
  @Public()
  @Post('register')
  async register(@Request() req) {
    return this.authService.create(req.body.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('delete/:id')
  deleteUser(@Param('id') id: string) {
    console.log(id);

    // return this.authService.delete(req.body.user);
  }
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
}
